(TeX-add-style-hook
 "bibliography"
 (lambda ()
   (LaTeX-add-bibitems
    "youtrack"
    "mercurial"
    "git"
    "visualstudio"
    "typescript"
    "less"
    "razor"
    "sitecore"
    "ajax"
    "pwa"
    "handlebars"
    "jquery"
    "gulp"))
 :latex)

