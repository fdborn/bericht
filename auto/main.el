(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("nag" "l2tabu" "orthodox") ("babel" "ngerman") ("fontenc" "T1") ("inputenc" "utf8") ("setspace" "onehalfspacing") ("geometry" "paper=a4paper")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (TeX-run-style-hooks
    "latex2e"
    "./tex/einleitung"
    "./tex/neveling"
    "./tex/verlauf"
    "./tex/bewertung"
    "nag"
    "scrreprt"
    "scrreprt10"
    "babel"
    "fontenc"
    "inputenc"
    "setspace"
    "microtype"
    "url"
    "geometry")
   (TeX-add-symbols
    '("techref" 2))
   (LaTeX-add-bibitems
    "youtrack"
    "mercurial"
    "git"
    "visualstudio"
    "typescript"
    "less"
    "razor"
    "sitecore"
    "ajax"
    "pwa"
    "handlebars"
    "jquery"
    "gulp"))
 :latex)

