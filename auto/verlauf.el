(TeX-add-style-hook
 "verlauf"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (LaTeX-add-labels
    "ch:verlauf"
    "subsec:youtrack"
    "subsec:mercurial"
    "subsec:visualstudio"
    "subsec:typescript"
    "subsec:less"
    "subsec:razor"
    "subsec:sitecore"
    "subsec:testsystems"
    "subsec:deployment"
    "subsec:ajax"
    "subsec:pwa"
    "subsec:handlebars"
    "subsec:jquery"
    "subsec:gulp"))
 :latex)

