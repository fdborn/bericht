(TeX-add-style-hook
 "bibliography"
 (lambda ()
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (LaTeX-add-bibitems
    "youtrack"
    "mercurial"
    "git"
    "visualstudio"
    "typescript"
    "less"
    "razor"
    "sitecore"
    "ajax"
    "pwa"
    "handlebars"
    "jquery"
    "gulp"))
 :latex)

